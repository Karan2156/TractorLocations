package com.trecker.challenge.trecker.model

class DriverTractor(
        val driver: Driver,
        val location: Location
)
