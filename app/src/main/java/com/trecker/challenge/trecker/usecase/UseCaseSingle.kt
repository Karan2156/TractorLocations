package com.trecker.challenge.trecker.usecase

import com.trecker.challenge.trecker.rx.RxSchedulers
import io.reactivex.Single
import io.reactivex.observers.DisposableSingleObserver

abstract class UseCaseSingle<Object> constructor(private val rxSchedulers: RxSchedulers) : UseCase() {

    internal abstract fun buildUseCaseObservable(): Single<Object>

    @Suppress("UNCHECKED_CAST")
    fun execute(observer: DisposableSingleObserver<Object>) {
        return try {
            dispose()
            val single = buildUseCaseObservable()
                    .subscribeOn(rxSchedulers.io)
                    .observeOn(rxSchedulers.androidMainThread)
            initDisposable(single.subscribeWith(observer))
        } catch (cce: ClassCastException) {
            throw ClassCastException("The use case encountered an error while building a signle user case.")
        }
    }

}