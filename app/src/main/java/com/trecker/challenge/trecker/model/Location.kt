package com.trecker.challenge.trecker.model

data class Location(
        val driver_id: String,
        val latitude: Double,
        val longitude: Double,
        val timestamp: String
)
