package com.trecker.challenge.trecker.repository

import com.trecker.challenge.trecker.model.Location
import com.trecker.challenge.trecker.network.TractorAPI
import io.reactivex.Single
import javax.inject.Inject

class LocationsRepositoryImpl @Inject constructor(
        private val tractorAPI: TractorAPI
) : LocationsRepository {

    override fun getDriverLocations(): Single<List<Location>> {
        return tractorAPI.driverLocations()
    }

}