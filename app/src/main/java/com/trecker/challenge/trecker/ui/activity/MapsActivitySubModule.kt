package com.trecker.challenge.trecker.ui.activity

import android.arch.lifecycle.ViewModel
import com.trecker.challenge.trecker.dagger.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class MapsActivitySubModule {

    @Binds
    @IntoMap
    @ViewModelKey(MapsActivityViewModel::class)
    abstract fun bindMapsActivityViewModel(mapsActivityViewModel: MapsActivityViewModel): ViewModel

}