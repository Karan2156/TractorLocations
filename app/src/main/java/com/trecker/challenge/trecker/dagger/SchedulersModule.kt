package com.trecker.challenge.trecker.dagger

import com.trecker.challenge.trecker.rx.RxSchedulers
import dagger.Module
import dagger.Provides
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

@Module
class SchedulersModule {

    @Provides
    fun providesSchedulers() = RxSchedulers(Schedulers.io(), AndroidSchedulers.mainThread())

}