package com.trecker.challenge.trecker.repository

import com.trecker.challenge.trecker.model.Driver
import com.trecker.challenge.trecker.network.TractorAPI
import io.reactivex.Single
import javax.inject.Inject

class DriversRepositoryImpl @Inject constructor(private val tractorAPI: TractorAPI) : DriversRepository {

    override fun getDrivers(): Single<List<Driver>> {
        return tractorAPI.drivers()
    }

}