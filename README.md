# Tractor drivers location

This projects inteds to show on a map the location and names of tractor drivers.

## Strategy

The architectural pattern chosen for this project was MVVM since it is easier to maintain, test, and later on, change. With the help of ViewModel of the Android Architecture Components and Dagger, this became easier to implement.

With this strategy, we can separate the View from the logic. This way the map activity is only responsible to update the view components from data provided by the ViewModel.

The ViewModel is responsible to get the data from the layers below using use cases, and expose this data via LiveData to the View. The use cases make testing easier by separating pieces of logic and test them independently. Same reasoning for using LiveData and Repository.

Repositories are used in order to hide the actual implementation. If we go back to the Clean Architecture principles, this repositories can be easily moved into a Domain module. If we change implementation to something else, the interface is kept, so the application should work as expected.

## Libraries

I used some extra libraries in order to help with the development of this solution.

### ViewModel/LiveData

From Google Architecture Components, this library integrates ViewModel and LiveData for implementing MVVM

### Dagger

Essential to build any MVVM application. This greatly helps in separating classes from each other making them easier to test independently.

### RxJava/RxAndroid

Adding Rx capabilities to the project, so we can use the helpful streams.

### Retrofit

This is one of the best Networking libraries for Android. Helps to fetch Data and turn it into Rx streams.

### Android Map Utils

This was introduced in order to include Clustering to the map. This way, if we have too many markers on the map, instead of populating the map with all these markers making the map really slow, we cluster them.

### Mockito Kotlin

Adding Mockito for testing purposes

### AssertJ

To make assertions easier and cleaner

### Core-testing

To help with testing LiveData
